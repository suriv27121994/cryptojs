const CryptoJS = require("crypto-js");
//let encrypted = CryptoJS.RC4Drop.encrypt("This Message", "Secret Passphrase");
let encrypted = CryptoJS.RC4Drop.encrypt("This Message", "Secret Passphrase", {
  drop: 3072 / 4,
});

let decrypted = CryptoJS.RC4Drop.decrypt(encrypted, "Secret Passphrase", {
  drop: 3072 / 4,
});

console.log(decrypted);