const CryptoJS = require("crypto-js");
let encrypted = CryptoJS.Rabbit.encrypt("The Message", "Secret Passphrase");
let decrypted = CryptoJS.Rabbit.decrypt(encrypted, "Secret Passphrase");

console.log(decrypted);