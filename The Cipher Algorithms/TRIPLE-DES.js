const CryptoJS = require("crypto-js");
const encrypted = CryptoJS.TripleDES.encrypt("Message Triple-DES algorithm", "12345");

const decrypted = CryptoJS.TripleDES.decrypt(encrypted, "12345");

//print encrypted
console.log(encrypted);
console.log(encrypted.toString());

//print decrypted
console.log(decrypted);
console.log(decrypted.toString());
