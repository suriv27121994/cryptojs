const CryptoJS = require("crypto-js");
const encrypted = CryptoJS.AES.encrypt("Message", "Secret Passphrase");

const decrypted = CryptoJS.AES.decrypt(encrypted, "Secret Passphrase");

//print encrypted
console.log(encrypted);
console.log(encrypted.toString());

//print decrypted
console.log(decrypted);
console.log(decrypted.toString()); 
