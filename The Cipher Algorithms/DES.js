const CryptoJS = require("crypto-js");
const encrypted = CryptoJS.DES.encrypt("Message DES algorithm", "12345");

const decrypted = CryptoJS.DES.decrypt(encrypted, "12345");

//print encrypted
console.log(encrypted);
console.log(encrypted.toString());

//print decrypted
console.log(decrypted);
console.log(decrypted.toString());
